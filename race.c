#include "csapp.h"

#define N 4

char *mensaje = "Salida del hilo X\n";
sem_t mutex;

void charatatime(char * str)
{
	char * ptr;
	int c;
	/* Ensure that characters sent to stdout are output as soon
	   as possible - make stdout unbuffered. */
	setbuf(stdout,NULL);
	P(&mutex);
	for(ptr = str; (c = *ptr++); ){
		usleep(1000);
		putc(c, stdout);
	}
	V(&mutex);
}

void *thread(void *varg)
{
	int len = strlen(mensaje);
	char *str = Malloc(len*sizeof(char));
	int num = *((int *) varg);
	
	strcpy(str,mensaje);
	str[len - 2] = 48 + num;
	charatatime(str);
	Free(str);
	Free(varg);
	return NULL;
}

int main()
{
	pthread_t tid;

	int *numero;
	Sem_init(&mutex,0,1);
	for (int i = 0;i < N; i++){
		numero = (int *)Malloc(sizeof(int));
		*numero=i;
		Pthread_create(&tid, NULL, thread, numero);
	}

	Pthread_exit(0);
}
